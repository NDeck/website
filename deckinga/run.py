from datetime import datetime, timedelta
from passlib.hash import bcrypt
from flask import (Flask, current_app, flash, make_response, render_template, redirect,
                   request, url_for)
from flask_login import LoginManager, current_user, login_required, login_user, logout_user
from flask_wtf.csrf import CSRFProtect

import pytz

from database import db
from forms import LoginUserForm, PasswordResetForm, RegisterUserForm, TroopMembershipForm
from models import CookieBooth, CookieTroop, PasswordReset, Role, User

import config

app = Flask(__name__)
app.config.from_object(config.ProductionConfig)
login_manager = LoginManager()
db.init_app(app)
login_manager.init_app(app)
login_manager.login_view = 'login'
csrf = CSRFProtect(app)
timezone = pytz.timezone("America/Chicago")


def get_week_ends(reserve_dt):
    # Get last second of reserve_dt
    end_date = datetime(reserve_dt.year, reserve_dt.month, reserve_dt.day,
                        tzinfo=timezone)
    end_this_week = end_date + timedelta(0, 86359)
    if end_this_week.weekday() < 4:
        while end_this_week.weekday() != 4:
            end_this_week += timedelta(days=1)
    if end_this_week.weekday() > 4:
        while end_this_week.weekday() != 4:
            end_this_week += timedelta(days=-1)
    end_next_week = end_this_week + timedelta(days=7)
    return end_this_week, end_next_week


def can_reserve(cookie_booth, troop, reserve_dt, end_this_week, end_next_week):
    # Booth has already been reserved
    if cookie_booth.troop_reserved:
        flash('That booth has already been reserved')
        return False

    # # This is the section for weekly reservations as done prior to 2021
    # # Booth is after the end of next week - can't reserve yet
    # if cookie_booth.starttime > end_next_week:
    #     flash('Booth is too far in the future to reserve')
    #     return False
    # # Booth is this week - all reservations allowed
    # if cookie_booth.starttime <= end_this_week:
    #     return True
    # # After 10pm on Wed - all reservations allowed
    # if (reserve_dt.weekday() == 2 and reserve_dt.hour >= 22) or reserve_dt.weekday() >= 3:
    #     return True
    # num_reserved = (CookieBooth.query.filter(CookieBooth.starttime >= end_this_week)
    #                                  .filter(CookieBooth.endtime < end_next_week)
    #                                  .filter(CookieBooth.troop_reserved == troop)
    #                                  .filter(CookieBooth.ignore_count == False)
    #                                  .count())
    # # Make their first reservation after 10pm Monday
    # if (((reserve_dt.weekday() == 0 and reserve_dt.hour >= 22)
    #         or (reserve_dt.weekday() == 1 and reserve_dt.hour < 22))
    #         and num_reserved == 0):
    #     return True
    # # Do they get a second early reservation
    # cookie_troop = CookieTroop.query.get(troop)
    # if (((reserve_dt.weekday() == 1 and reserve_dt.hour >= 22)
    #         or (reserve_dt.weekday() == 2 and reserve_dt.hour < 22))
    #         and num_reserved < cookie_troop.num_early_booths):
    #     return True

    # Are we in open reservations (free for all)
    open_dt = datetime.strptime('2021-01-18 19:00:00-0600', '%Y-%m-%d %H:%M:%S%z')
    if (reserve_dt > open_dt):
        return True
    # Has this troop reserved the max they are allowed to reserve early
    cookie_troop = CookieTroop.query.get(troop)
    num_reserved = (CookieBooth.query.filter(CookieBooth.troop_reserved == troop)
                                     .filter(CookieBooth.ignore_count == False)
                                     .count())
    if (num_reserved < cookie_troop.num_early_booths):
        return True
    flash('You can not reserve a booth at this time.')
    return False


def reserve_booth(troop, booth, reserve_dt):
    cookie_booth = CookieBooth.query.get(booth)
    end_this_week, end_next_week = get_week_ends(reserve_dt)
    if can_reserve(cookie_booth, troop, reserve_dt, end_this_week, end_next_week):
        cookie_booth.troop_reserved = troop
        cookie_booth.reserved_datetime = reserve_dt
        db.session.commit()
        flash('Booth reserved')


@app.route('/')
def homepage():
    return render_template('index.html')


@app.route('/resume/')
def resume():
    return render_template('resume.html')


@app.route('/analysis/')
def analysis():
    flash("Flash Test!")
    return render_template('analysis.html')


@app.route('/apps/')
def apps():
    return render_template('apps.html')


@app.route('/cookie_home/')
def cookie_home():
    return render_template('cookie_home.html')


@app.route('/cookie_release_booth/', methods=['GET', 'POST'])
def cookie_release_booth():
    if request.method == 'POST':
        booth_id = request.form.get('booth_selected')
        release_booth = (CookieBooth.query.get(booth_id))
        release_booth.troop_reserved = None
        release_booth.reserved_datetime = None
        db.session.commit()
        flash('Booth released for someone else to reserve.')
    troops = [x.id for x in current_user.get_troops()]
    booths = (CookieBooth.query.filter(CookieBooth.troop_reserved.in_(troops))
                               .filter(CookieBooth.endtime >= datetime.utcnow())
                               .order_by(CookieBooth.starttime)
                               .all())
    return render_template('cookie_release_booth.html', booths=booths, timezone=timezone)


@app.route('/cookie_reserve_booth/', methods=['GET', 'POST'])
def cookie_reserve_booth():
    if request.method == 'POST':
        troop = request.form.get('troop_selected')
        booth = request.form.get('booth_selected')
        if troop is None:
            flash('You must select a troop to reserve this booth')
        if booth is None:
            flash('No booth selected')
        if (troop and booth):
            reserve_dt = datetime.now(timezone)
            reserve_booth(troop, booth, reserve_dt)
    troops = current_user.get_troops()
    start = datetime.utcnow()
    end = start + timedelta(days=9)
    end = end.replace(hour=6, minute=0, second=0, microsecond=0)
    # Despite the warning - SQLAlchemy needs this to be == None instead of is None
    booths = (CookieBooth.query.filter(CookieBooth.endtime >= start)
                               .filter(CookieBooth.endtime < end)
                               .filter(CookieBooth.troop_reserved == None)
                               .order_by(CookieBooth.starttime)
                               .all())
    dates = set()
    locations = set()
    for i in booths:
        dates.add(i.starttime.astimezone(timezone).date())
    for i in booths:
        locations.add(i.location.name)
    dates = sorted(list(dates))
    locations = sorted(list(locations))
    return render_template('cookie_reserve_booth.html', booths=booths, troops=troops,
                           dates=dates, locations=locations, timezone=timezone)


@app.route('/cookie_view_my_booth/')
def cookie_view_my_booth():
    troops = current_user.get_troops()
    troop_ids = list()
    for t in troops:
        troop_ids.append(t.id)
    start = datetime.utcnow()
#    _, end = get_week_ends(start)
    booths = (CookieBooth.query.filter(CookieBooth.endtime >= start)
#                               .filter(CookieBooth.endtime <= end)
                               .filter(CookieBooth.troop_reserved.in_(troop_ids))
                               .order_by(CookieBooth.starttime)
                               .all())
    dates = set()
    locations = set()
    for i in booths:
        dates.add(i.starttime.astimezone(timezone).date())
    for i in booths:
        locations.add(i.location.name)
    dates = sorted(list(dates))
    locations = sorted(list(locations))
    return render_template('cookie_view_booth.html', booths=booths,
                           dates=dates, locations=locations, timezone=timezone)


@app.route('/cookie_view_all_reserved/')
def cookie_view_all_reserved():
    year_start = datetime(datetime.now().year, 1, 1, 0, 0)
    booths = (CookieBooth.query.filter(CookieBooth.troop_reserved != None)
                               .filter(CookieBooth.starttime > year_start)
                               .order_by(CookieBooth.starttime)
                               .all())
    dates = set()
    locations = set()
    for i in booths:
        dates.add(i.starttime.astimezone(timezone).date())
    for i in booths:
        locations.add(i.location.name)
    dates = sorted(list(dates))
    locations = sorted(list(locations))
    return render_template('cookie_view_all_reserved.html', booths=booths,
                           dates=dates, locations=locations, timezone=timezone)


@app.route('/cookie_view_booth/')
def cookie_view_booth():
    start = datetime.utcnow()
#    _, end = get_week_ends(start)
    booths = (CookieBooth.query.filter(CookieBooth.endtime >= start)
#                               .filter(CookieBooth.endtime <= end)
                               .order_by(CookieBooth.starttime)
                               .all())
    dates = set()
    locations = set()
    for i in booths:
        dates.add(i.starttime.astimezone(timezone).date())
    for i in booths:
        locations.add(i.location.name)
    dates = sorted(list(dates))
    locations = sorted(list(locations))
    return render_template('cookie_view_booth.html', booths=booths,
                           dates=dates, locations=locations, timezone=timezone)


@app.route('/cookie_user_mgmt', methods=['GET', 'POST'])
def cookie_user_mgmt():
    users = (User.query.filter(Role.name.in_(['cookie_user']))
                       .order_by(User.email)
                       .all())
    troops = (CookieTroop.query.order_by(CookieTroop.troop_number)
                               .all())
    form = TroopMembershipForm(request.form)
    form.user.choices = [(u.id, u.email) for u in users]
    form.troop.choices = [(t.id, t.troop_number) for t in troops]
    if form.validate_on_submit():
        moduser = User.query.get(request.form.get('user'))
        moduser.troop_membership = []
        for i in request.form.getlist('troop'):
            troop = CookieTroop.query.get(i)
            moduser.troop_membership.append(troop)
        db.session.commit()
        flash('Troop membership updated for that user.')
    return render_template('/cookie_user_mgmt.html', form=form)


@app.route('/register/', methods=['GET', 'POST'])
def register():
    form = RegisterUserForm(request.form)
    if form.validate_on_submit():
        email = form.email.data.lower()
        password = bcrypt.hash(form.password.data)
        existing_user = User.query.filter_by(email=email).count()
        if existing_user != 0:
            flash('That email address already exists.')
            return render_template('register.html', form=form)
        else:
            user = User(email, password)
            role = Role.query.filter_by(name='cookie_user').first()
            user.roles.append(role)
            db.session.add(user)
            db.session.commit()
        return redirect(url_for('login'))
    return render_template('register.html', form=form)


@app.route('/login/', methods=['GET', 'POST'])
def login():
    form = LoginUserForm(request.form)
    if form.validate_on_submit():
        email = form.email.data.lower()
        remember_me = form.remember_me.data
        user = User.query.filter_by(email=email).first()
        if user and bcrypt.verify(form.password.data, user.password):
            login_user(user, remember=remember_me)
            flash("Successfully logged in")
            user.last_login_at = datetime.utcnow()
            user.last_login_ip = request.environ['REMOTE_ADDR']
            user.login_count += 1
            db.session.commit()
            return redirect(url_for('cookie_home'))
        else:
            flash("Invalid Credentials")
    return render_template('login.html', form=form)


@app.route('/logout/')
@login_required
def logout():
    logout_user()
    flash("You have been logged out")
    return redirect(url_for('login'))


@app.route('/password_reset/', methods=['GET', 'POST'])
def password_reset():
    form = PasswordResetForm(request.form)
    if form.validate_on_submit():
        email = form.email.data.lower()
        reset_token = form.reset_token.data
        user = User.query.filter_by(email=email).first()
        reset = PasswordReset.query.filter_by(reset_token=reset_token).first()
        password = bcrypt.hash(form.password.data)
        if not user:
            flash('Email address invalid')
            return redirect(url_for('password_reset'))
        if not reset:
            flash('Reset token invalid')
            return redirect(url_for('password_reset'))
        if reset.user_id == user.id:
            user.password = password
            db.session.delete(reset)
            db.session.commit()
            flash('Password reset')
            return redirect(url_for('login'))
        else:
            flash('Reset token does not match user')
            return redirect(url_for('password_reset'))
    return render_template('password_reset.html', form=form)


@login_manager.user_loader
def load_user(user_id):
    return User.query.filter_by(id=user_id).first()


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html')


@app.errorhandler(405)
def method_not_found(e):
    return render_template('405.html')


@app.errorhandler(500)
def ineternal_error(e):
    return render_template('500.html')


@app.route('/sitemap.xml', methods=['GET'])
def sitemap():
    """Generate sitemap.xml. Makes a list of urls and date modified."""
    pages = list()
    ten_days_ago = (datetime.now() - timedelta(days=10)).date().isoformat()
    # static pages
    for rule in current_app.url_map.iter_rules():
        if "GET" in rule.methods and len(rule.arguments) == 0:
            pages.append([rule.rule, ten_days_ago])
    sitemap_xml = render_template('sitemap.xml', pages=pages)
    response = make_response(sitemap_xml)
    response.headers["Content-Type"] = "application/xml"
    return response


@app.route('/robots.txt')
def robots():
    return(app.config['ROBOTS_TXT'])


if __name__ == '__main__':
    app.run()
