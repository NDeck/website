from datetime import datetime
from database import db
from sqlalchemy.dialects import postgresql as pg

user_role = db.Table('user_role',
                     db.Column('id', db.Integer, primary_key=True),
                     db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
                     db.Column('role_id', db.Integer, db.ForeignKey('role.id')))


troop_member = db.Table('cookie_troop_member',
                        db.Column('id', db.Integer, primary_key=True),
                        db.Column('cookie_troop_id', db.Integer, db.ForeignKey('cookie_troop.id')),
                        db.Column('user_id', db.Integer, db.ForeignKey('user.id')))


class User(db.Model):
    __tablename__ = 'user'
    id = db.Column('id', db.Integer, primary_key=True)
    email = db.Column('email', db.String(256))
    password = db.Column('password', db.String(256))
    active = db.Column('active', db.Boolean())
    confirmed_at = db.Column('confirmed_at', db.DateTime)
    last_login_at = db.Column('last_login_at', db.DateTime)
    last_login_ip = db.Column('last_login_ip', pg.INET)
    login_count = db.Column('login_count', db.Integer)
    roles = db.relationship('Role', secondary=user_role,
                            backref=db.backref('users', lazy='subquery'))
    troop_membership = db.relationship('CookieTroop', secondary=troop_member,
                                       backref=db.backref('users', lazy='subquery'))

    def __init__(self, email, password):
        self.email = email
        self.password = password
        self.active = True
        self.confirmed_at = datetime.now()
        self.login_count = 0

    def is_authenticated(self):
        return True

    def is_active(self):
        return self.active

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def get_roles(self):
        my_roles = list()
        for role in Role.query.filter(Role.users.any(id=self.get_id())).all():
            my_roles.append(role.name)
        return my_roles

    def get_troops(self):
        my_troops = list()
        for troop in CookieTroop.query.filter(CookieTroop.users.any(id=self.get_id())).all():
            my_troops.append(troop)
        return my_troops


class Role(db.Model):
    __tablename__ = 'role'
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('name', db.String(50))
    description = db.Column('description', db.String(500))


class CookieTroop(db.Model):
    __tablename__ = 'cookie_troop'
    id = db.Column('id', db.Integer, primary_key=True)
    troop_number = db.Column('troop_number', db.Integer)
    description = db.Column('description', db.String(500))
    num_early_booths = db.Column('num_early_booths', db.Integer)
    booths = db.relationship('CookieBooth', backref='troop', lazy='dynamic')


class CookieBoothLocation(db.Model):
    __tablename__ = 'cookie_booth_location'
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column('booth_name', db.String(200))
    street_number = db.Column('booth_street_number', db.String(15))
    street = db.Column('booth_street', db.String(50))
    city = db.Column('booth_city', db.String(50))
    notes = db.Column('booth_notes', db.String(500))
    booth_instances = db.relationship('CookieBooth', backref='location', lazy='joined')


class CookieBooth(db.Model):
    __tablename__ = 'cookie_booth_timeslot'
    id = db.Column('id', db.Integer, primary_key=True)
    location_id = db.Column('booth_location_id', db.Integer,
                            db.ForeignKey('cookie_booth_location.id'))
    starttime = db.Column('booth_starttime', db.DateTime)
    endtime = db.Column('booth_endtime', db.DateTime)
    notes = db.Column('booth_notes', db.String(500))
    troop_reserved = db.Column('troop_reserved_id', db.Integer,
                               db.ForeignKey('cookie_troop.id'))
    reserved_datetime = db.Column('reserved_datetime', db.DateTime)
    ignore_count = db.Column('ignore_count', db.Boolean())


class PasswordReset(db.Model):
    __tablename__ = 'password_reset'
    id = db.Column('id', db.Integer, primary_key=True)
    user_id = db.Column('user_id', db.Integer,
                        db.ForeignKey('user.id'))
    reset_hash = db.Column('reset_hash', pg.UUID)
    created_date = db.Column('created_date', db.DateTime)
