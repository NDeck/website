-- Create a way to reset passwords
CREATE TABLE public.password_reset (
	id serial not null,
	user_id integer not null,
	reset_hash uuid not null,
	created_dt date not null default now(),
	CONSTRAINT password_reset_pkey PRIMARY KEY (id),
	CONSTRAINT fk_password_reset_user FOREIGN KEY (user_id) REFERENCES public.user(id)
)
WITH (
	OIDS=FALSE
) ;

alter table cookie_booth_location
	add column phone_number character varying(20);
alter table cookie_booth_location
	add column manager_name character varying(50);

alter table public.cookie_booth_timeslot
	add column ignore_count boolean default false;