-- Now let's create the base roles.
-- I'll need one for a super admin (me)
-- I'll need the cookie booth user and admin

INSERT INTO "role" ("name", description)
VALUES('site_admin', 'Super user and admin for the entire website');

INSERT INTO "role" ("name", description)
VALUES('cookie_admin', 'Admin for all cookie booth functions');

INSERT INTO "role" ("name", description)
VALUES('cookie_user', 'Authorized troop representative');


-- Need to create the troop table structure to hold troop associations
-- Table: public.cookie_troop
-- DROP TABLE public.cookie_troop;

CREATE TABLE public.cookie_troop
(
    id serial NOT NULL,
    troop_number integer,
    description character varying(500) COLLATE pg_catalog."default",
    CONSTRAINT cookie_troop_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.cookie_troop
    OWNER to website;

-- Table: public.cookie_troop_member
-- DROP TABLE public.cookie_troop_member;

CREATE TABLE public.cookie_troop_member
(
    id serial NOT NULL,
    cookie_troop_id integer,
    user_id integer,
    CONSTRAINT cookie_troop_member_pkey PRIMARY KEY (id),
    CONSTRAINT fk_cookie_troop_member_cookie_troop FOREIGN KEY (cookie_troop_id)
        REFERENCES public.cookie_troop (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_cookie_troop_member_user FOREIGN KEY (user_id)
        REFERENCES public."user" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.cookie_troop_member
    OWNER to website;

alter table public.user drop column current_login_at;
alter table public.user drop column current_login_ip;

/* It doesn't do much good to have users if there aren't any booths */

-- Table: public.cookie_booth_location
-- DROP TABLE public.cookie_booth_location;

CREATE TABLE public.cookie_booth_location
(
    id serial NOT NULL,
    booth_name character varying(200),
    booth_street_number character varying(15),
    booth_street character varying(50),
    booth_city character varying(50),
    booth_notes character varying(500),
    CONSTRAINT cookie_booth_location_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.cookie_booth_location
    OWNER to website;

-- Table: public.cookie_booth_timeslot
-- DROP TABLE public.cookie_booth_timeslot;

CREATE TABLE public.cookie_booth_timeslot
(
    id serial NOT NULL,
    booth_location_id integer,
    booth_starttime timestamp with time zone,
    booth_endtime timestamp with time zone,
    booth_notes character varying(500),
    troop_reserved_id integer,
    reserved_datetime timestamp with time zone,
    CONSTRAINT cookie_booth_timeslot_pkey PRIMARY KEY (id),
    CONSTRAINT fk_cookie_booth_timeslot_location FOREIGN KEY (booth_location_id)
        REFERENCES public.cookie_booth_location (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_cookie_booth_timeslot_reservation FOREIGN KEY (troop_reserved_id)
        REFERENCES public.cookie_troop (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.cookie_booth_timeslot
    OWNER to website;