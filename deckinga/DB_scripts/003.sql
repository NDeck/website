alter table cookie_troop
	add column is_large_troop boolean;
alter table cookie_troop
	add constraint uk_cookie_troop_number 
    unique (troop_number);


insert into cookie_troop (troop_number, is_large_troop) values (900, True);
insert into cookie_troop (troop_number, is_large_troop) values (2069, True);
insert into cookie_troop (troop_number, description, is_large_troop) values (1, 'Large troop test', True);
insert into cookie_troop (troop_number, description, is_large_troop) values (2, 'Small troop test', False);

INSERT INTO public.cookie_booth_location(booth_name, booth_street_number, booth_street, booth_city, booth_notes)
	VALUES ('Test Site 1', '123', 'Main St.', 'Leander', 'Set up to the left only');
INSERT INTO public.cookie_booth_location(booth_name, booth_street_number, booth_street, booth_city, booth_notes)
	VALUES ('Test Site 2', '456', 'Elm St.', 'Cedar Park', 'Make sure to talk to the manager when leaving');
INSERT INTO public.cookie_booth_location(booth_name, booth_street_number, booth_street, booth_city)
	VALUES ('Test Site 3', '789', 'Apple Ct.', 'Leander');
INSERT INTO cookie_booth_timeslot(booth_location_id, booth_starttime, booth_endtime)
	select id, '2017-09-28 19:00:00-05', '2017-09-28 21:00:00-05'
    from cookie_booth_location;
INSERT INTO cookie_booth_timeslot(booth_location_id, booth_starttime, booth_endtime)
	select booth_location_id, max(booth_starttime) + interval '1 day', max(booth_endtime) + interval '1 day'
    from cookie_booth_timeslot
    group by booth_location_id;


alter table cookie_troop
	drop column is_large_troop;
alter table cookie_troop
	add column num_early_booths integer;