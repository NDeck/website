import os

web_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))


class Config(object):
    DEBUG = False
    TESTING = False
    LOGGER_NAME = 'DeckingaComLog.txt'
    WTF_CSRF_ENABLED = True


class ProductionConfig(Config):
    SECRET_KEY = 'Something Sutably Hard For Production'
    WTF_CSRF_SECRET_KEY = SECRET_KEY
    LOGGER_HANDLER_POLICY = 'production'
    SQLALCHEMY_DATABASE_URI = "postgresql://website:password@server:5432/website"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    ROBOTS_TXT = "User-agent: *\nDisallow: /register/\nDisallow: /login/"


class DevelopmentConfig(Config):
    DEBUG = True
    SECRET_KEY = 'Easy-WhoCares?'
    WTF_CSRF_SECRET_KEY = SECRET_KEY
    EXPLAIN_TEMPLATE_LOADING = False
    SQLALCHEMY_DATABASE_URI = "postgresql://website:password@localhost:5432/website"
#    SQLALCHEMY_DATABASE_URI = "sqlite:////{}/website.db".format(web_dir)
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    ROBOTS_TXT = "User-agent: *\nDisallow: /"


class RunConfig(DevelopmentConfig):
    SOLVE_CONFLICT = True
