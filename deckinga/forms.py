from flask_wtf import FlaskForm
from wtforms import BooleanField, PasswordField, SelectField, SelectMultipleField, StringField
from wtforms.validators import DataRequired, InputRequired, Length, EqualTo
from wtforms.widgets import CheckboxInput, TableWidget


class RegisterUserForm(FlaskForm):
    email = StringField('e-mail address',
                        [DataRequired(), Length(min=6, max=100)])
    password = PasswordField('password',
                             [DataRequired(), Length(min=3, max=50)])
    confirm_password = PasswordField('confirm_password',
                                     [EqualTo('password', message="Passwords must match")])
    accept_box = BooleanField('I will be most excellent to everyone',
                              [InputRequired()])


class LoginUserForm(FlaskForm):
    email = StringField('e-mail address')
    password = PasswordField('password')
    remember_me = BooleanField('Remember Me')


class TroopMembershipForm(FlaskForm):
    user = SelectField('Troop Leader', validators=[DataRequired()], id='select_leader', coerce=int)
    troop = SelectMultipleField('Manage booths for the following troops', id='select_troops',
                                widget=TableWidget(), option_widget=CheckboxInput(), coerce=int)


class PasswordResetForm(FlaskForm):
    email = StringField('e-mail address',
                        [DataRequired(), Length(min=6, max=100)])
    reset_token = StringField('reset token', [DataRequired()])
    password = PasswordField('password',
                             [DataRequired(), Length(min=3, max=50)])
    confirm_password = PasswordField('confirm_password',
                                     [EqualTo('password', message="Passwords must match")])
